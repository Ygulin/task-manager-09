package ru.tsc.gulin.tm.service;

import ru.tsc.gulin.tm.api.ICommandRepository;
import ru.tsc.gulin.tm.api.ICommandService;
import ru.tsc.gulin.tm.model.Command;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
