package ru.tsc.gulin.tm.api;

public interface ICommandController {
    void showSystemInfo();

    void showWelcome();

    void showErrorArgument(String arg);

    void showErrorCommand(String arg);

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();
}
